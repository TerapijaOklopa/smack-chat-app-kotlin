package com.smackchat

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_create_user.*
import java.util.*

class CreateUserActivity : AppCompatActivity() {

    var userAvatar = "profileDefault"
    var avatarColor = "[0.5, 0.5, 0.5, 1]"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)
    }

    fun createUserBtnClicked(view: View) {
        Toast.makeText(this, "createUserBtnClicked", Toast.LENGTH_SHORT).show()
    }

    fun backgroundColorBtnClicked(view: View) {
        createAvatarImageView.setBackgroundColor(createBackground())
    }

    fun generateUserAvatar(view: View) {
        createAvatarImageView.setImageResource(createUserAvatar())
    }

    private fun createUserAvatar(): Int {
        val random = Random()
        val color = random.nextInt(2)
        val avatar = random.nextInt(28)

        userAvatar = if (color == 0) {
            "light$avatar"
        } else {
            "dark$avatar"
        }
        return resources.getIdentifier(userAvatar, "drawable", packageName)
    }

    private fun createBackground(): Int {
        val random = Random()
        val r = random.nextInt(255)
        val g = random.nextInt(255)
        val b = random.nextInt(255)

        avatarColor = "[${r.toDouble() / 255}, ${g.toDouble() / 255}, ${b.toDouble() / 255}, 1"
        return Color.rgb(r, g, b)
    }
}
