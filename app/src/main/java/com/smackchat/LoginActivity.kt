package com.smackchat

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun loginLoginBtnClicked(view: View) {
        Toast.makeText(this, "loginLoginBtnClicked", Toast.LENGTH_SHORT).show()

    }

    fun loginSignUpBtnClicked(view: View) {
        startActivity(Intent(this, CreateUserActivity::class.java))
    }
}
